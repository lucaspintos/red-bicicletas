var Bike = require('../models/bike');

exports.bike_list = function (req, res) {

    console.log('bike_list ### ' + Bike.allBikes);

    res.render('bikes/index', { bikes: Bike.allBikes, title: 'Bike List' });
}

exports.bike_create_get = function (req, res) {

    res.render('bikes/create', { title: 'Add Bike' });
}

exports.bike_create_post = function (req, res) {

    let ubication = [req.body.latitude, req.body.longitude];
    let bike = new Bike(req.body.id, req.body.color, req.body.model, ubication);

    Bike.add(bike);

    console.log('bike_create_post ### ' + Bike.allBikes);

    res.redirect('/bikes');
}

exports.bike_update_get = function (req, res) {

    let bike = Bike.findById(req.params.id);

    res.render('bikes/update', { bike, title: 'Edit bike' });
}

exports.bike_update_post = function (req, res) {

    let bike = Bike.findById(req.params.id);
    
    bike.id = req.body.id;
    bike.model = req.body.model;
    bike.color = req.body.color;
    bike.ubication = [req.body.latitude, req.body.longitude];
    
    res.redirect('/bikes');
}

exports.bike_delete_post = function(req, res) {
    Bike.removeById(req.body.id);
    
    res.redirect('/bikes');
}