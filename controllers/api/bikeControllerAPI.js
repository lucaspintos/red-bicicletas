var Bike = require('../../models/bike');

exports.bike_list = function (req, res) {
    res.status(200).json({
        bikes: Bike.allBikes
    });
}

exports.bike_create = function (req, res) {
    let ubication = [req.body.latitude, req.body.longitude];
    let bike = new Bike(req.body.id, req.body.color, req.body.model, ubication);

    Bike.add(bike);

    res.status(200).json({
        bike: bike
    });
}

exports.bike_delete = function (req, res) {
    Bike.removeById(req.body.id);
    res.status(204).send();
}

exports.bike_update = function (req, res) {
    let bike = Bike.findById(req.params.id);
    
    bike.id = req.body.id;
    bike.model = req.body.model;
    bike.color = req.body.color;
    bike.ubication = [req.body.latitude, req.body.longitude];

    res.status(200).json({
        bike: bike
    });
}