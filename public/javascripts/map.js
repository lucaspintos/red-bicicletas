var map = L.map('main_map').setView([-34.821609, -55.957615], 15);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibHVjYXNwaW50b3M5MDkiLCJhIjoiY2tvbTZham11MDNsbDJycW5oenZhbjdoaiJ9.gud3uljjG-RqN6NhoAmVtg'
}).addTo(map);

//L.marker([-34.821609, -55.957615]).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bikes',
    success: function(result) {
        console.log(result);
        result.bikes.forEach(function(bike) {

            L.marker(bike.ubication, {title: `# ${bike.id} / Modelo: ${bike.model} / Color: ${bike.color}`}).addTo(map);

        });
    }
});