var Bike = function (id, color, model, ubication){
    this.id = id;
    this.color = color;
    this.model = model;
    this.ubication = ubication;
}

Bike.prototype.toString = function (){
    return `id: ${this.id}, color: ${this.color}, model: ${this.model}, ubication: ${this.ubication}`;
}

Bike.allBikes = [];
Bike.add = function(bike){
    Bike.allBikes.push(bike);
}

Bike.findById = function(bikeId){
    let bike = Bike.allBikes.find(bike => bike.id == bikeId);

    if(bike) {
        return bike;
    }else{
        throw new Error(`No existe una bicicleta con el id ${bikeId}`);
    }
}

Bike.removeById = function(bikeId){
    let bike = Bike.findById(bikeId);

    for (let index = 0; index < Bike.allBikes.length; index++) {
        if(Bike.allBikes[index].id == bikeId) {
            Bike.allBikes.splice(index, 1);
            break;
        }
    }
}

let bike1 = new Bike(4234123, 'Roja', 'Urbana', [-34.819462, -55.958141]);
let bike2 = new Bike(3423121, 'Verde', 'Montaña', [-34.821543, -55.958128]);

Bike.add(bike1);
Bike.add(bike2);

module.exports = Bike;